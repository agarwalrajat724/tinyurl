# URL Shortener Application

App Url :- localhost:8080/index.html

Requirements Given:-

1) No of Hits
2) Time Series Graph of the Hits
3) API Rate Limiter
4) URL Shortener Services

URL Shortener Services:-

POST API :- /api/v1/urls
    createShortURL
    params :- originalUrl (Mandatory)
    
    Returns: Url Entity
    
    tinyUrlKey : Unique 7 Character Key for the original Url
    scheme: Protocol of the Url (http or https)
    host : Domain
    userId: (Optional) User Id
    originalUrl: original url
    createdOn: Date the Tiny Url is created
    expiresOn: As now expiry date is 2 Years after the creation Date 
    
    Errors:-
    Invalid URL:- For Invalid URL
    URL Already Exists :-  if the tiny url has already been created for the provided original url
    
GET API :- /api/v1/urls/{url}
    Query param: TinyUrl Key
    Redirects to the Original Url Page
    

Scale Assumption:-
-> For 7 Characters long Tiny Url Key 3.5 Trillion Unique Strings are possible
-> For 1000 req/sec will take 110 Years to consume all

URL Shortener Approach

Generate URL's through Key Generation Service

-> Key Generation Service (KGS) that generates required length (seven letter) strings beforehand using Long Counter to 
   keep track of the last consumed Counter and stores them in a database.    
-> Whenever we want to shorten a URL, we will just take one of the already-generated keys and use it.
-> We won’t have to worry about duplications or collisions. KGS will make sure all the keys inserted into key-DB are unique
-> Also, once the used keys are expired, can be pulled back to the Key-DB

Concurrency Problems ?
If there are multiple servers reading keys concurrently, we might get a scenario where two or more servers try to read 
the same key from the database. How can we solve this concurrency problem?
Solution:- We will mark the keys used in the database of Key Generation Service.

DB Clean UP
-> Whenever a user tries to access an expired link, we can delete the link and return an error to the user.
-> A separate Cleanup service can run periodically to remove expired links from our storage and cache.
-> We can have a default expiration time for each link (e.g., two years).
-> After removing an expired link, we can put the key back in the key-DB to be reused.




RATE LIMITER

API Method Level Custom Annotation @Throttling can be used with below params:-

ThrottlingType -> IPAddress, UserId
Limit -> Rate Limit
TimeUnit -> Seconds, Minutes, Hours

To configure all the api rate limits through a configured file, solution in ThrottleInterceptor can
be extended.
@Throttling(throttlingType = ThrottlingType.REMOTE_ADDR, limit = 6l, timeUnit = TimeUnit.MINUTES) 

Used Sliding Window Algorithm to implement API Rate Limiter

We can maintain a sliding window if we can keep track of each request per IpAddress/UserId.
We can store the timestamp of each request in a Redis Sorted Set in our ‘value’ field of hash-table.

KEY                 :       VALUE
IPAddress/UserId           SortedSet<UnixTimeStamp>

Let’s assume our rate limiter is allowing three requests per minute per user, 
-> Remove all the timestamps from the Sorted Set that are older than “CurrentTime - 1 minute”.
-> Count the total number of elements in the sorted set. Reject the request if this count is greater
   than our throttling limit of “3”.
-> Insert the current time in the sorted set and accept the request.


Metrics:-

All the metrics are pushed to in memory java Map at a scheduled interval of 1 Minute
Using Spring's @Scheduled or ScheduledExecutorService 


OTHER DESIGN CONSIDERATIONS:-
-> Cache URLs that are frequently accessed using Distributed Cache, which can store full URLs with their respective Short URL
-> The application servers, before hitting backend storage, can quickly check if the cache has the desired URL.
Eviction Policy -> Can use LRU (Least Recently Used) policy


NOTE: Graph Metrics will be updated at an interval of 1 Minute



    
