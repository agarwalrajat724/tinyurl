package com.yellow.messenger.tinyurl.web.model;

public enum ThrottlingType {
    REMOTE_ADDR, USER_ID
}
