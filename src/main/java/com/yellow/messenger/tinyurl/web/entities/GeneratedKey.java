package com.yellow.messenger.tinyurl.web.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GeneratedKey {

    private String key;

    private Long counter;

    private boolean used;
}
