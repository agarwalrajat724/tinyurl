package com.yellow.messenger.tinyurl.web.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.concurrent.TimeUnit;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Throttle {

    private ThrottlingType throttlingType;

    private long limit;

    private TimeUnit timeUnit;
}
