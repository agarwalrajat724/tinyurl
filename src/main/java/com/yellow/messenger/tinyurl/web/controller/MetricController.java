package com.yellow.messenger.tinyurl.web.controller;

import com.yellow.messenger.tinyurl.web.model.MetricsResponse;
import com.yellow.messenger.tinyurl.service.IMetricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/metrics")
public class MetricController {

    @Autowired
    @Qualifier("micrometerMetricService")
    private IMetricService micrometerMetricService;

    /**
     *
     * @return
     */
    @GetMapping(value = {"/", ""})
    public MetricsResponse metrics() {
        return new MetricsResponse(micrometerMetricService.getGraphData(), micrometerMetricService.getMetricsByApi(),
                micrometerMetricService.totalHits(), micrometerMetricService.totalRedirects());
    }
}
