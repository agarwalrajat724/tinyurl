package com.yellow.messenger.tinyurl.web.interceptor;

import com.yellow.messenger.tinyurl.web.annotation.Throttling;
import com.yellow.messenger.tinyurl.web.exception.ThrottlingException;
import com.yellow.messenger.tinyurl.service.IRateLimiterService;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Slf4j
public class ThrottleInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private IRateLimiterService rateLimiterService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Throttling throttling = handlerMethod.getMethod().getAnnotation(Throttling.class);
            if (null != throttling) {
                String ipAddress = getRemoteAddr(request);
                if (rateLimiterService.rateLimit(ipAddress, DateTime.now().getMillis(),
                        throttling.limit(), throttling.timeUnit())) {
                    response.setStatus(HttpStatus.TOO_MANY_REQUESTS.value());
                    response.setContentType("application/json");
                    response.getWriter().write(new ThrottlingException("Too Many Requests").getMessage());
                    return false;
                }
                response.addHeader("X-RateLimit-Limit", String.valueOf(throttling.limit()));
            }
        }
        return super.preHandle(request, response, handler);
    }

    private String getRemoteAddr(HttpServletRequest request) {
        String ipFromHeader = request.getHeader("X-FORWARDED-FOR");
        if (ipFromHeader != null && ipFromHeader.length() > 0) {
            log.debug("ip from proxy - X-FORWARDED-FOR : " + ipFromHeader);
            return ipFromHeader;
        }
        return request.getRemoteAddr();
    }
}
