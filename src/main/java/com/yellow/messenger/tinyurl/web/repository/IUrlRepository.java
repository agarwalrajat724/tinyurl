package com.yellow.messenger.tinyurl.web.repository;

import com.yellow.messenger.tinyurl.web.entities.Url;

import java.util.Optional;

public interface IUrlRepository {

    Optional<Url> findByOriginalUrl(String originalUrl);

    Optional<Url> findByTinyUrl(String tinyUrl);

    Url save(Url url);
}
