package com.yellow.messenger.tinyurl.web.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private String id;

    private String name;

    private CharSequence password;

    private CharSequence email;

    private Date createdOn;

    private Date lastLogin;
}
