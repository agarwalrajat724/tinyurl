package com.yellow.messenger.tinyurl.web.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Url {

    private String tinyUrl;

    private String scheme;

    private String host;

    private String userId;

    private String originalUrl;

    private Date createdOn;

    private Date expiresOn;
}
