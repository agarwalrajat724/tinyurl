package com.yellow.messenger.tinyurl.web.repository.impl;

import com.yellow.messenger.tinyurl.web.entities.Url;
import com.yellow.messenger.tinyurl.web.repository.IUrlRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class UrlRepository implements IUrlRepository {

    private final Map<String, Url> tinyUrlMap;

    public UrlRepository() {
        this.tinyUrlMap = new ConcurrentHashMap<>();
    }

    @Override
    public Optional<Url> findByOriginalUrl(String originalUrl) {
        List<Url> urls = new ArrayList<>(tinyUrlMap.values());
        return urls.stream().filter(url -> url.getOriginalUrl().equals(originalUrl)).findFirst();
    }

    @Override
    public Optional<Url> findByTinyUrl(String tinyUrl) {
        return tinyUrlMap.containsKey(tinyUrl) ? Optional.of(tinyUrlMap.get(tinyUrl)) : Optional.empty();
    }

    @Override
    public Url save(Url url) {
        tinyUrlMap.put(url.getTinyUrl(), url);
        return tinyUrlMap.get(url.getTinyUrl());
    }
}
