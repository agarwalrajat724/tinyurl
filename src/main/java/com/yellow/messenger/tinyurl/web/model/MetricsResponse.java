package com.yellow.messenger.tinyurl.web.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.concurrent.ConcurrentMap;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MetricsResponse {

    private Object[][] graph;

    private ConcurrentMap<String, ConcurrentMap<Integer, Integer>> metricApiMap;

    private long totalHits;

    private long totalRedirects;
}
