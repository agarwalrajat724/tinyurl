package com.yellow.messenger.tinyurl.web.interceptor;

import com.yellow.messenger.tinyurl.configuration.MetricsConfig;
import com.yellow.messenger.tinyurl.service.IMetricService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Slf4j
public class MetricsInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private MetricsConfig metricsConfig;

    @Autowired
    @Qualifier("micrometerMetricService")
    private IMetricService micrometerMetricService;

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        int status = response.getStatus();

        String req = request.getMethod() + " " + request.getRequestURI();
        micrometerMetricService.increaseCount(req, status, response, ex);
        super.afterCompletion(request, response, handler, ex);
    }
}
