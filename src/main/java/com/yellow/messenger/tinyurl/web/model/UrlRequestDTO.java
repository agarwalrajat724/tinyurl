package com.yellow.messenger.tinyurl.web.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class UrlRequestDTO {

    @NotEmpty(message = "Original Url Can not be Null Or Empty")
    private String originalUrl;

    private String userId;
}
