package com.yellow.messenger.tinyurl.web.controller;

import com.yellow.messenger.tinyurl.web.annotation.Throttling;
import com.yellow.messenger.tinyurl.web.exception.UrlAlreadyExistsException;
import com.yellow.messenger.tinyurl.web.model.UrlRequestDTO;
import com.yellow.messenger.tinyurl.web.model.ThrottlingType;
import com.yellow.messenger.tinyurl.web.entities.Url;
import com.yellow.messenger.tinyurl.web.model.UrlResponseDTO;
import com.yellow.messenger.tinyurl.service.IUrlShortenerService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Controller
@RequestMapping("/api/v1/urls")
public class UrlShortenerController {


    private final IUrlShortenerService urlService;

    private final ModelMapper modelMapper;

    @Autowired
    public UrlShortenerController(IUrlShortenerService urlService) {
        this.urlService = urlService;
        this.modelMapper = new ModelMapper();
    }

    /**
     *
     * @param urlRequestDTO
     * @param request
     * @param response
     * @return
     * @throws UrlAlreadyExistsException
     * @throws URISyntaxException
     */
    @Throttling(throttlingType = ThrottlingType.REMOTE_ADDR, limit = 6l, timeUnit = TimeUnit.MINUTES)
    @PostMapping(value = {"/", ""})
    public @ResponseBody UrlResponseDTO createShortUrl(@RequestBody @Valid UrlRequestDTO urlRequestDTO, HttpServletRequest request, HttpServletResponse response) throws UrlAlreadyExistsException, URISyntaxException {
        Url url = urlService.insert(urlRequestDTO.getOriginalUrl(), request.getRequestURL().toString());
        return modelMapper.map(url, UrlResponseDTO.class);
    }


    /**
     *
     * @param tinyUrl
     * @param request
     * @param response
     * @return
     */
    @GetMapping(value = "/{url}", produces = "text/html")
    public ModelAndView get(@PathVariable("url") String tinyUrl, HttpServletRequest request, HttpServletResponse response) {
        Optional<Url> url = urlService.find(tinyUrl);
        if (url.isPresent()) {
            response.setHeader("Location", url.get().getOriginalUrl());
            response.setStatus(HttpServletResponse.SC_MOVED_PERMANENTLY);
            return new ModelAndView(new RedirectView(url.get().getOriginalUrl(), true));
        } else {
            ModelMap model = new ModelMap();
            model.put("uri", request.getRequestURL());
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return new ModelAndView("404", model);
        }
    }
}
