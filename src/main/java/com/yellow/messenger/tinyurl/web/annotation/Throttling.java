package com.yellow.messenger.tinyurl.web.annotation;

import com.yellow.messenger.tinyurl.web.model.ThrottlingType;
import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Throttling {

    ThrottlingType throttlingType() default ThrottlingType.REMOTE_ADDR;

    long limit() default 5l;

    TimeUnit timeUnit() default TimeUnit.SECONDS;
}

