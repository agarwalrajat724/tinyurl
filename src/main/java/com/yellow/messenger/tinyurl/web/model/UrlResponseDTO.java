package com.yellow.messenger.tinyurl.web.model;

import lombok.Data;

import java.util.Date;

@Data
public class UrlResponseDTO {

    private String tinyUrl;

    private String scheme;

    private String host;

    private String userId;

    private String originalUrl;

    private Date createdOn;

    private Date expiresOn;
}
