package com.yellow.messenger.tinyurl.service.impl;

import com.yellow.messenger.tinyurl.web.exception.InvalidUrlException;
import com.yellow.messenger.tinyurl.web.exception.UrlAlreadyExistsException;
import com.yellow.messenger.tinyurl.web.entities.Url;
import com.yellow.messenger.tinyurl.web.repository.IUrlRepository;
import com.yellow.messenger.tinyurl.service.IKeyGenerationService;
import com.yellow.messenger.tinyurl.service.IUrlShortenerService;
import com.yellow.messenger.tinyurl.util.UrlValidator;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

@Service
public class UrlShortenerService implements IUrlShortenerService {

    private final IUrlRepository urlRepository;

    private final IKeyGenerationService keyGenerationService;

    @Autowired
    public UrlShortenerService(IUrlRepository urlRepository, IKeyGenerationService keyGenerationService) {
        this.urlRepository = urlRepository;
        this.keyGenerationService = keyGenerationService;
    }

    /**
     *
     * @param originalUrl
     * @param requestUrl
     * @return
     * @throws UrlAlreadyExistsException
     * @throws URISyntaxException
     */
    public Url insert(String originalUrl, String requestUrl) throws UrlAlreadyExistsException, URISyntaxException {

        if (!UrlValidator.validate(originalUrl)) {
            throw new InvalidUrlException("Invalid Url : " + originalUrl);
        }
        urlRepository.findByOriginalUrl(originalUrl).ifPresent(url -> {
            throw new UrlAlreadyExistsException(" Tiny Url already Exists : " + requestUrl + "/" + url.getTinyUrl());
        });

        URI uri = new URI(originalUrl);
        String token = keyGenerationService.generateKey().getKey();

        Url url = Url.builder()
                .scheme(uri.getScheme())
                .host(uri.getHost())
                .originalUrl(originalUrl)
                .tinyUrl(token)
                .createdOn(DateTime.now().toDate())
                .expiresOn(DateTime.now().plusYears(2).toDate())
                .build();

        return urlRepository.save(url);
    }

    /**
     *
     * @param tinyUrl
     * @return
     */
    public Optional<Url> find(String tinyUrl) {
        return urlRepository.findByTinyUrl(tinyUrl);
    }
}
