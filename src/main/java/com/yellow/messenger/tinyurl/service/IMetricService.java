package com.yellow.messenger.tinyurl.service;

import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.ConcurrentMap;

public interface IMetricService {

    void increaseCount(final String request, final int status, final HttpServletResponse response, final Exception ex);

    Object[][] getGraphData();

    ConcurrentMap<String, ConcurrentMap<Integer, Integer>> getMetricsByApi();

    long totalHits();

    long totalRedirects();
}
