package com.yellow.messenger.tinyurl.service.impl;

import com.yellow.messenger.tinyurl.service.ICounterService;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicLong;

@Service
public class CounterService implements ICounterService {

    private AtomicLong atomicLong;

    public CounterService() {
        this.atomicLong = new AtomicLong();
    }

    @Override
    public long nextCounter() {
        return atomicLong.incrementAndGet();
    }
}
