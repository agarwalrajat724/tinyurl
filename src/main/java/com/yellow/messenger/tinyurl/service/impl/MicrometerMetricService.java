package com.yellow.messenger.tinyurl.service.impl;

import com.yellow.messenger.tinyurl.configuration.MetricsConfig;
import com.yellow.messenger.tinyurl.service.IMetricService;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.search.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

@Service
public class MicrometerMetricService implements IMetricService {

    @Autowired
    private MeterRegistry registry;

    @Autowired
    private MetricsConfig metricsConfig;

    private final ConcurrentMap<String, ConcurrentMap<Integer, Integer>> metricApiMap;
    private final List<List<Integer>> statusMetricsByMinute;
    private final Set<String> statusList;
//    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
    private static final String COUNTER_STATUS_REGISTRY = "counter.status.";

    public MicrometerMetricService() {
        super();
        this.statusMetricsByMinute = new CopyOnWriteArrayList<>();
        this.statusList = new CopyOnWriteArraySet<>();
        this.metricApiMap = new ConcurrentHashMap<>();
    }

    // API

    @Override
    public void increaseCount(final String request, final int status, HttpServletResponse response, Exception ex) {
        final String statusCountKey = COUNTER_STATUS_REGISTRY + status;
        registry.counter(statusCountKey).increment(1);
        statusList.add(statusCountKey);

        ConcurrentMap<Integer, Integer> statusMap = metricApiMap.getOrDefault(request, new ConcurrentHashMap<>());
        Integer count = statusMap.getOrDefault(status, 0);
        statusMap.put(status, ++count);
        metricApiMap.put(request, statusMap);

        metricsConfig.getTotalHits().increment();

        if (status > 499 && status < 600) {
            metricsConfig.getInternalServerError().increment();
        } else if (status == response.SC_NOT_FOUND) {
            metricsConfig.getNotFound().increment();
        } else if (status == response.SC_BAD_REQUEST) {
            metricsConfig.getBadRequest().increment();
        } else if (status == response.SC_MOVED_PERMANENTLY || status == response.SC_MOVED_TEMPORARILY) {
            metricsConfig.getRedirects().increment();
        } if (ex != null) {
            metricsConfig.getError().increment();
        }
    }

    @Override
    public Object[][] getGraphData() {
        final Date current = new Date();
        final int colCount = statusList.size() + 1;
        final int rowCount = statusMetricsByMinute.size() + 1;
        final Object[][] result = new Object[rowCount][colCount];
        result[0][0] = "Time";
        int value = 1;
        for (final String status : statusList) {
            result[0][value] = HttpStatus.valueOf(Integer.parseInt(status.substring(status.length() - 3, status.length()))).toString();
            value++;
        }

        for (int rowNo = 1; rowNo < rowCount; rowNo++) {
            result[rowNo][0] = dateFormat.format(new Date(current.getTime() - (60000 * (rowCount - rowNo))));
        }

        List<Integer> minuteOfStatuses;
        for (int rowNo = 1; rowNo < rowCount; rowNo++) {
            minuteOfStatuses = statusMetricsByMinute.get(rowNo - 1);
            for (value = 1; value <= minuteOfStatuses.size(); value++) {
                result[rowNo][value] = minuteOfStatuses.get(value - 1);
            }
            while (value < colCount) {
                result[rowNo][value] = 0;
                value++;
            }
        }
        return result;
    }

    @Override
    public ConcurrentMap<String, ConcurrentMap<Integer, Integer>> getMetricsByApi() {
        return metricApiMap;
    }

    @Override
    public long totalHits() {
        return Double.valueOf(metricsConfig.getTotalHits().count()).longValue();
    }

    @Override
    public long totalRedirects() {
        return Double.valueOf(metricsConfig.getRedirects().count()).longValue();
    }

    // Non - API

    @Scheduled(fixedDelay = 60000)
    private void exportMetrics() {
        final ArrayList<Integer> statusCount = new ArrayList<>();
        for (final String status : statusList) {
            Search search = registry.find(status);
            if (search != null && null != search.counter()) {
                Counter counter = search.counter();
                statusCount.add(Double.valueOf(counter.count()).intValue());
                registry.remove(counter);
            } else {
                statusCount.add(0);
            }
        }
        statusMetricsByMinute.add(statusCount);
    }
}
