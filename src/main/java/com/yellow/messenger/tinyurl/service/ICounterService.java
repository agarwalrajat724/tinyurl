package com.yellow.messenger.tinyurl.service;

public interface ICounterService {

    long nextCounter();
}
