package com.yellow.messenger.tinyurl.service.impl;

import com.yellow.messenger.tinyurl.web.entities.GeneratedKey;
import com.yellow.messenger.tinyurl.service.ICounterService;
import com.yellow.messenger.tinyurl.service.IKeyGenerationService;
import com.yellow.messenger.tinyurl.util.Base62Converter;
import org.springframework.stereotype.Service;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;


@Service
public class KeyGenerationService implements IKeyGenerationService {


    private ICounterService counterService;

    private Queue<GeneratedKey> queue;

    private static final int DEFAULT_LIMIT = 10;

    public KeyGenerationService(ICounterService counterService) {
        this.counterService = counterService;
        this.queue = new ConcurrentLinkedQueue<>();
    }

    @Override
    public GeneratedKey generateKey() {
        if (queue.isEmpty()) {
            while (queue.size() != DEFAULT_LIMIT) {
                long counter = counterService.nextCounter();
                queue.offer(new GeneratedKey(Base62Converter.getInstance().createUniqueID(counter), counter,
                        false));
            }
        }
        GeneratedKey generatedKey = queue.poll();
        generatedKey.setUsed(true);
        return generatedKey;
    }
}
