package com.yellow.messenger.tinyurl.service.impl;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.yellow.messenger.tinyurl.service.IRateLimiterService;
import org.springframework.stereotype.Service;

import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

@Service
public class ApiRateLimiterService implements IRateLimiterService {

    private final Cache<String, SortedSet<Long>> apiRateLimiter;

    public ApiRateLimiterService() {
        this.apiRateLimiter = CacheBuilder.newBuilder()
                .concurrencyLevel(10)
                .maximumSize(100)
                .softValues()
                .expireAfterAccess(30, TimeUnit.MINUTES)
                .build();
    }

    @Override
    public boolean rateLimit(String ipAddress, long timeStamp, long rateLimit, TimeUnit timeUnit) {
        long timeStampDiff = slidingWindowTimestampDiff(timeUnit);
        SortedSet<Long> timeStampSet = null;
        synchronized (ipAddress.intern()) {
            timeStampSet = apiRateLimiter.getIfPresent(ipAddress);
            if (null == timeStampSet) {
                timeStampSet = new TreeSet<>();
            }
            while (!timeStampSet.isEmpty() && timeStampSet.first() < (timeStamp - timeStampDiff)) {
                timeStampSet.remove(timeStampSet.first());
            }
            if (timeStampSet.size() > rateLimit) {
                return true;
            }
            timeStampSet.add(timeStamp);
            apiRateLimiter.put(ipAddress, timeStampSet);
        }
        return timeStampSet.size() > rateLimit;
    }

    private long slidingWindowTimestampDiff(TimeUnit timeUnit) {
        long timeStampDiff = 0l;
        if (TimeUnit.SECONDS.equals(timeUnit)) {
            timeStampDiff = 1000;
        } else if (TimeUnit.MINUTES.equals(timeUnit)) {
            timeStampDiff = 60 * 1000;
        } else if (TimeUnit.HOURS.equals(timeUnit)) {
            timeStampDiff = 60 * 60 * 1000;
        }
        return timeStampDiff;
    }
}
