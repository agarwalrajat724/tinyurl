package com.yellow.messenger.tinyurl.service;

import com.yellow.messenger.tinyurl.web.exception.UrlAlreadyExistsException;
import com.yellow.messenger.tinyurl.web.entities.Url;

import java.net.URISyntaxException;
import java.util.Optional;

public interface IUrlShortenerService {

    Url insert(String originalUrl, String requestUrl) throws UrlAlreadyExistsException, URISyntaxException;

    Optional<Url> find(String tinyUrl);
}
