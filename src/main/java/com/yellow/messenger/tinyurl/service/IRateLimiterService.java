package com.yellow.messenger.tinyurl.service;

import java.util.concurrent.TimeUnit;

public interface IRateLimiterService {

    boolean rateLimit(String param, long timeStamp, long rateLimit, TimeUnit timeUnit);
}
