package com.yellow.messenger.tinyurl.service;

import com.yellow.messenger.tinyurl.web.entities.GeneratedKey;

public interface IKeyGenerationService {

    GeneratedKey generateKey();
}
