package com.yellow.messenger.tinyurl.configuration;

import io.micrometer.core.aop.TimedAspect;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@Getter
public class MetricsConfig {

    private final Counter error;

    private final Counter internalServerError;

    private final Counter notFound;

    private final Counter badRequest;

    private final Counter redirects;

    private final Counter totalHits;

    private final String serviceName;

    public MetricsConfig(@Value("${spring.application.name}") String serviceName) {
        this.serviceName = serviceName;
        this.error = Metrics.counter("app.exception.count");
        this.internalServerError = Metrics.counter("app.internalerror.count");
        this.notFound = Metrics.counter("app.notfound.count");
        this.badRequest = Metrics.counter("app.badrequest.count");
        this.redirects = Metrics.counter("app.redirect.count");
        this.totalHits = Metrics.counter("counter.total.hits");
    }


    @Bean
    public TimedAspect timedAspect(MeterRegistry registry) {
        return new TimedAspect(registry);
    }

}
