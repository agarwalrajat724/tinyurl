package com.yellow.messenger.tinyurl.configuration;

import com.yellow.messenger.tinyurl.web.interceptor.MetricsInterceptor;
import com.yellow.messenger.tinyurl.web.interceptor.ThrottleInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Bean
    public ThrottleInterceptor throttleInterceptor() {
        return new ThrottleInterceptor();
    }


    @Bean
    public MetricsInterceptor metricsInterceptor() {
        return new MetricsInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(metricsInterceptor());
        registry.addInterceptor(throttleInterceptor());
    }
}

