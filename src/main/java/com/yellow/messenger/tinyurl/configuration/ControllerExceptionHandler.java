package com.yellow.messenger.tinyurl.configuration;

import com.yellow.messenger.tinyurl.web.exception.InvalidUrlException;
import com.yellow.messenger.tinyurl.web.exception.ThrottlingException;
import com.yellow.messenger.tinyurl.web.exception.UrlAlreadyExistsException;
import com.yellow.messenger.tinyurl.web.model.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(UrlAlreadyExistsException.class)
    public final ResponseEntity<Object> handleUrlAlreadyExistsException(UrlAlreadyExistsException ex, WebRequest webRequest) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorResponse error = new ErrorResponse("Tiny Url Exists : ", details);
        return new ResponseEntity(error, HttpStatus.CONFLICT);
    }


    @ExceptionHandler(InvalidUrlException.class)
    public final ResponseEntity<Object> handleInvalidUrlException(InvalidUrlException ex, WebRequest webRequest) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorResponse error = new ErrorResponse("Invalid Url : ", details);
        return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ThrottlingException.class)
    public final ResponseEntity<Object> handleThrottleException(ThrottlingException ex, WebRequest webRequest) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorResponse error = new ErrorResponse("Too Many Requests : ", details);
        return new ResponseEntity(error, HttpStatus.TOO_MANY_REQUESTS);
    }
}
