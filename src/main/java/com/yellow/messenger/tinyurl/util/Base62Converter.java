package com.yellow.messenger.tinyurl.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Base62Converter {

    private static volatile Base62Converter INSTANCE = null;

    private static Map<Character, Integer> charToIndexTable;
    private static List<Character> indexToCharTable;

    private Base62Converter() {
        initializeCharToIndexTable();
        initializeIndexToCharTable();
    }


    public static Base62Converter getInstance() {
        if (null == INSTANCE) {
            synchronized (Base62Converter.class) {
                if (null == INSTANCE) {
                    INSTANCE = new Base62Converter();
                }
            }
        }
        return INSTANCE;
    }

    private void initializeCharToIndexTable() {
        charToIndexTable = new HashMap<>();
        for (int i = 0; i < 26; ++i) {
            char c = 'a';
            c += i;
            charToIndexTable.put(c, i);
        }
        for (int i = 26; i < 52; ++i) {
            char c = 'A';
            c += (i - 26);
            charToIndexTable.put(c, i);
        }
        for (int i = 52; i < 62; ++i) {
            char c = '0';
            c += (i - 52);
            charToIndexTable.put(c, i);
        }
//		charToIndexTable.put('+', 62);
//        charToIndexTable.put('/', 63);
    }

    private void initializeIndexToCharTable() {
        indexToCharTable = new ArrayList<>();
        for (int i = 0; i < 26; ++i) {
            char c = 'a';
            c += i;
            indexToCharTable.add(c);
        }
        for (int i = 26; i < 52; ++i) {
            char c = 'A';
            c += (i - 26);
            indexToCharTable.add(c);
        }
        for (int i = 52; i < 62; ++i) {
            char c = '0';
            c += (i - 52);
            indexToCharTable.add(c);
        }
    }

    public String createUniqueID(Long id) {
        List<Integer> base62ID = convertBase10ToBase62ID(id);
        StringBuilder uniqueURLID = new StringBuilder();
        for (int digit : base62ID) {
            uniqueURLID.append(indexToCharTable.get(digit));
        }
        while (uniqueURLID.length() < 7) {
            uniqueURLID.append(0);
        }
        return uniqueURLID.reverse().toString();
    }

    private List<Integer> convertBase10ToBase62ID(Long id) {
        List<Integer> digits = new ArrayList<>();
        while (id > 0l) {
            long remainder = (id % 62);
            digits.add(Long.valueOf(remainder).intValue());
            id /= 62;
        }
        return digits;
    }

    public Long getDictionaryKeyFromUniqueID(String uniqueID) {
        List<Character> base62Number = new ArrayList<>();
        for (int i = 0; i < uniqueID.length(); ++i) {
            base62Number.add(uniqueID.charAt(i));
        }
        Long dictionaryKey = convertBase62ToBase10ID(base62Number);
        return dictionaryKey;
    }

    private Long convertBase62ToBase10ID(List<Character> ids) {
        long id = 0L;
        int exp = ids.size() - 1;
        for (int i = 0; i < ids.size(); ++i, --exp) {
            int base10 = charToIndexTable.get(ids.get(i));
            id += (base10 * Math.pow(62.0, exp));
        }
        return id;
    }
}
